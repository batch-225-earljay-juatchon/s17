/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/

function printWelcomeMesseges() {
	let fullName = prompt("Enter your Fullname");
	console.log("Hello," + " " + fullName);
	let age = prompt("Enter your Age");
	console.log("You are" + " " + age + " " + "Years" + " " + "old");
	let location = prompt("Enter your Location");
	console.log("You live in" + " "  + location);	
}
printWelcomeMesseges();

	//first function here:

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.	
*/
function printName() {
	console.log("1. Eraeserheads");
	console.log("2. Mayonaise");
	console.log("3. Urband");
	console.log("4. Bandang Lapis");
	console.log("5. I Belong to the Zoo")
}
printName();

	//second function here:

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.	
*/
function printMovies() {
	console.log("1. Avengers: Endgame Rotten Tomatoes Rating: 94%");
	console.log("2. Psycho Rotten Tomatoes Rating: 95%");
	console.log("3. Shawshank Redemption Rotten Tomatoes Rating: 92%");
	console.log("4. To Kill A Mockingbird Rotten Tomatoes Rating: 93%");
	console.log("5. The God Father Rotten Tomatoes Rating: 96%");
}
printMovies();
	
	//third function here:

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let Jen = prompt("Enter your first friend's name:"); 
	let Cris = prompt("Enter your second friend's name:"); 
	let Ellise = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(Jen); 
	console.log(Cris); 
	console.log(Ellise); 
}
printUsers();

